# wiseflow.nix
Flake for wiseflow-device-monitor

This flake sets up a few things:
- Installs custom versions of the executables `gnome-screenshot` and `xwininfo` that doesn't do anything
- Modifies `wiseflow-device-monitor` to be linked with nix libraries
- Installs a proxy used for verification of the modified executable

## Setup
Install all tools required to run wiseflow-device-monitor
```
nix develop gitlab:jo1gi/wiseflow.nix
```

Before running wiseflow-device-monitor, run
```
wiseflow-proxy
```

This will create a proxy used when wiseflow-device-monitor tried to verify the
integrity of the program. This is required because the flake modifies the
executable and therefore doesn't has the correct checksum.

Afterwards you can run `wiseflow-device-monitor` in another shell to start the program.
