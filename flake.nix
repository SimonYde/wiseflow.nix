{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };

  outputs =
    { self, nixpkgs, ... }:
    let
      pkgs = nixpkgs.legacyPackages."x86_64-linux";
    in
    {
      packages.x86_64-linux = {

        wiseflow-device-monitor = pkgs.stdenv.mkDerivation rec {
          pname = "wiseflow-device-monitor";
          version = "2.3.0";

          src = builtins.fetchurl {
            url = "https://uniwise-wisemonitor.s3.eu-west-1.amazonaws.com/${version}/wiseflow_device_monitor_${version}_linux.deb";
            sha256 = "1nd3h4harbqk9cmhc757vy5sffiw6v9cvhbdmhclnjwsdgzp119l";
          };

          nativeBuildInputs = with pkgs; [
            autoPatchelfHook
            dpkg
          ];

          buildInputs = with pkgs; [
            webkitgtk
            xorg.xwininfo
          ];

          installPhase = ''
            mkdir -p $out/tmp
            dpkg -x $src $out/tmp
            cp -avr $out/tmp/usr/* $out
            rm -rf $out/tmp
            sed -i 's/Terminal=false/Terminal=true/g' $out/share/applications/wiseflow-device-monitor.desktop
          '';
        };

        fake-gnome-screenshot = pkgs.callPackage ./nix/fake-gnome-screenshot.nix { };

        fake-xwininfo = pkgs.callPackage ./nix/fake-xwininfo.nix { };

        wiseflow-proxy = pkgs.callPackage ./nix/wiseflow-proxy { };

        default = self.packages.x86_64-linux.wiseflow-device-monitor;
      };

      devShells.x86_64-linux.default = pkgs.mkShell {
        buildInputs = [
          self.packages.x86_64-linux.wiseflow-device-monitor
          self.packages.x86_64-linux.fake-gnome-screenshot
          self.packages.x86_64-linux.fake-xwininfo
          self.packages.x86_64-linux.wiseflow-proxy
        ];
        shellHook = ''
          export SSL_CERT_FILE="$HOME/.mitmproxy/mitmproxy-ca.pem";
          export https_proxy="http://127.0.0.1:9976/";
        '';
      };
    };
}
