from mitmproxy import http

class PassIntegrity:
    auth_url = "https://api.eu.wiseflow.io/device-monitor/v1/auth/integrity"

    def response(self, flow: http.HTTPFlow) -> None:
        if flow.request.pretty_url == self.auth_url:
            flow.response = http.Response.make(
                200,
                b'{"versionOk":true,"checksumOk":true}',
                {
                    "Content-Type": "application/json; charset=UTF-8",
                }
            )

addons = [PassIntegrity()]
