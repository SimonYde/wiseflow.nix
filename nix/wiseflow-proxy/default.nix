{ pkgs, ... }:
pkgs.stdenv.mkDerivation {
  pname = "wiseflow-proxy";
  version = "1.0.0";

  src = ./.;

  installPhase = ''
    mkdir -p $out/bin
    mkdir -p $out/share/wiseflow-proxy
    cp $src/wiseflow-proxy.py $out/share/wiseflow-proxy/wiseflow-proxy.py
    echo "${pkgs.mitmproxy}/bin/mitmdump -s "$out/share/wiseflow-proxy/wiseflow-proxy.py" -p 9976" > $out/bin/wiseflow-proxy
    chmod +x $out/bin/wiseflow-proxy
  '';
}
