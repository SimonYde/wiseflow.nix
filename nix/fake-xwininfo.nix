{ pkgs, ... }:

pkgs.writeShellScriptBin "xwininfo" ''
  echo "$@" >> xwininfo.log
''
