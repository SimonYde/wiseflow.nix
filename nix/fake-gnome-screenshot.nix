{ pkgs, ... }:

pkgs.writeShellScriptBin "gnome-screenshot" ''
  last_arg="''${@: -1}"
  ${pkgs.imagemagick}/bin/convert -size 100x100 canvas:white "PNG:$last_arg"
  echo "$@" >> screenshot.log
''
